class Saldo {
	constructor() {
		this.SaldoRetiro = 0;
		this.SaldoTotal = 0;
		this.SaldoConsulta = 0;
	}
		GetSaldoRetiro() {
			return this.SaldoRetiro
		}
		GetSaldoTotal() {
			return this.SaldoTotal
		}
		GetSaldoConsulta() {	
			return this.SaldoConsulta
		}
		SetSaldoRetiro(value) {
			this.SaldoRetiro = value
		}
		SetSaldoTotal (value) {
			this.SaldoTotal = value
		}
		SetSaldoConsulta(value) {
			this.SaldoConsulta = value
		}

}
module.exports = Saldo;
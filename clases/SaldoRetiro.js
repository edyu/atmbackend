const Saldo = require ('./Saldo')
const UpdateUser = require ('../updateUser').default
class SaldoRetiro extends Saldo {
	constructor() {
		super()
		this.monto = 0;
		this.cantidad = 0;
	}
	GetMonto () {
		return this.monto
	}
	GetCantidad(){
		return this.cantidad
	}
	SetMonto (value) {
		this.monto = value
	}
	SetCantidad() {
		this.cantidad = value
	}
 	RetiroSaldo(fourCard, montoADebitar) {
 		let jsonPersona = UpdateUser(fourCard, montoADebitar);
 		return jsonPersona;
	}
	VerificarSaldoTotal(){
		return true
	}
}
module.exports = SaldoRetiro;

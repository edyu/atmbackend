const ATM = require ('../clases/ATM');
const Cuenta = require ('../clases/Cuenta');
const SaldoRetiro = require ('../clases/SaldoRetiro');

function ConsultaDeSaldo (req, res) {
	let digitos = req.body.digitos;
	let retiro = req.body.retiro;
	console.log(digitos);
	let saldoRetiro = new SaldoRetiro();
	let cuenta = new Cuenta(saldoRetiro);
	let atm = new ATM(cuenta)
	let monto = atm.cuenta.saldo.RetiroSaldo(digitos, retiro);
	res.json({monto : retiro });
}

module.exports = ConsultaDeSaldo;
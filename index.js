const Express = require ("express");
const app = Express();
const FindUser = require('./findUser').default; 
const bodyParser = require('body-parser');
const routes = require('./rutas');

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

app.use(bodyParser.json());

routes(app)

app.listen (3001, () => console.log('listo el servidor' ));